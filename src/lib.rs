use std::fmt::{self, Debug, Display};

pub type AppResult<P, E> = core::result::Result<P, AppErrors<E>>;

pub trait Error {
    fn get_message(&self) -> String;
    fn replace(&mut self, new_msg: String);
}

trait ErrorTrait<P, E>
where
    E: Default + Debug + Clone + Display + Error,
{
    fn log(self) -> Self;
    fn warn(self) -> Self;
    fn option(self) -> AppResult<Option<P>, E>;
}
impl<P, E> ErrorTrait<P, E> for AppResult<P, E>
where
    E: Default + Debug + Clone + Display + Error,
{
    fn warn(self) -> Self {
        if let Err(e) = &self {
            log::warn!("{}", e.to_string());
        }
        self
    }

    fn log(self) -> Self {
        if let Err(e) = &self {
            log::error!("{}", e.to_string());
        }
        self
    }

    fn option(self) -> AppResult<Option<P>, E> {
        Ok(self.ok())
    }
}

#[derive(Default, Clone)]
pub struct AppErrors<E>(Vec<E>)
where
    E: Default + Debug + Clone + Display + Error;

impl<E> std::error::Error for AppErrors<E> where E: Default + Debug + Clone + Display + Error {}
impl<E> AppErrors<E>
where
    E: Default + Debug + Clone + Display + Error,
{
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn append(&mut self, other: &mut Self) {
        self.0.append(&mut other.0)
    }

    pub fn from(err: E) -> Self {
        Self(vec![err])
    }
    // TODO could be nice to have sorted fmt by E kind
}
impl<E> core::ops::Deref for AppErrors<E>
where
    E: Default + Debug + Clone + Display + Error,
{
    type Target = Vec<E>;

    fn deref(self: &'_ Self) -> &'_ Self::Target {
        &self.0
    }
}
impl<E> core::ops::DerefMut for AppErrors<E>
where
    E: Default + Debug + Clone + Display + Error,
{
    fn deref_mut(self: &'_ mut Self) -> &'_ mut Self::Target {
        &mut self.0
    }
}
impl<E> fmt::Display for AppErrors<E>
where
    E: Default + Debug + Clone + Display + Error,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.len() != 1 {
            writeln!(f, "{} errors found (details below)", self.len())?;
            for err in self.iter() {
                writeln!(f, "\t{}", err.to_string())?;
            }
        } else {
            write!(f, "{}", self.first().unwrap().to_string())?;
        }
        Ok(())
    }
}
// A unique format for dubugging output
impl<E> fmt::Debug for AppErrors<E>
where
    E: Default + Debug + Clone + Display + Error,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.len() != 1 {
            writeln!(f, "{} errors found (debug details below)", self.len())?;
            for err in self.iter() {
                writeln!(f, "\t{:#?}", err)?;
            }
        } else {
            write!(f, "{:#?}", self.first().unwrap().to_string())?;
        }
        Ok(())
    }
}

#[macro_export]
macro_rules! err {
    ($error:expr) => {{
        AppErrors::from($error)
    }};
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn it_works() {
//         let result = add(2, 2);
//         assert_eq!(result, 4);
//     }
// }
